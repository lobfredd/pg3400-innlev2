#ifndef CHARLIST
#define CHARLIST
#define MAX_INT_STRING_LENGTH 11
#include <stdio.h>

typedef struct CharList CharList;
struct CharList{
	char* array;
	int length;
	int count;
	void (*add)(CharList*, char);
	int (*spaceLeft)(CharList*);
	void (*addIntAsString)(CharList*, char*, int);
};

CharList *initCharList(int length);
void freeCharList(CharList *self);
char *detachCharArrayAndFree(CharList *self);

#endif
