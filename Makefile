ChatGUI = cChat
CryptoLib = cCrypto
CrackerLib = cCracker
EncryptionGUI = cCipher
CrackerGUI = cCracker

CFLAGS = -Wall -Werror -std=c11 -g

utilSrc = src/datastructures/charlist.c \
src/datastructures/intlist.c \
src/datastructures/stringutil.c \
src/datastructures/stringlist.c

CryptoLibSRC = src/libs/ccrypto.c $(utilSrc)
CrackLibSRC = src/libs/cracker.c $(CryptoLibSRC)

utilObj = charlist.o intlist.o stringutil.o stringlist.o
CryptoLibObj = ccrypto.o $(utilObj)
CrackerLibObj = cracker.o

all: cciphercmd ccrackercmd server client ccipher ccracker cchat clean

clean:
	$(RM) *.o *.a

#Executeables
cciphercmd: src/cciphercmd.c cryptolib
	gcc -L. $(CFLAGS) src/cciphercmd.c $(utilSrc) -o cciphercmd -l$(CryptoLib)

ccrackercmd: src/ccrackercmd.c crackerlib
	gcc -L. $(CFLAGS) src/ccrackercmd.c $(utilSrc) -o ccrackercmd -l$(CrackerLib)

server: src/socket/server.c cryptolib
	gcc -L. -pthread $(CFLAGS) -o server src/socket/server.c src/socket/socketutil.c -l$(CryptoLib)


#GUI executeables
ccipher: src/ccipher.c cryptolib
	gcc -L. $(CFLAGS) src/ccipher.c src/gtkutils.c -o $(EncryptionGUI) `pkg-config --cflags --libs gtk+-3.0` -l$(CryptoLib) -lpthread

ccracker: src/ccracker.c crackerlib
	gcc -L. $(CFLAGS) src/ccracker.c src/gtkutils.c -o $(CrackerGUI) `pkg-config --cflags --libs gtk+-3.0` -l$(CrackerLib) -lpthread

cchat: src/socket/cchat.c cryptolib src/socket/client.c
	gcc -L. $(CFLAGS) src/socket/cchat.c src/socket/client.c src/socket/socketutil.c src/gtkutils.c -o $(ChatGUI) `pkg-config --cflags --libs gtk+-3.0` -l$(CryptoLib) -lpthread


#Libs
cryptolib: $(CryptoLibObj)
	ar rcs lib$(CryptoLib).a $(CryptoLibObj)

$(CryptoLibObj): $(CryptoLibSRC)
	gcc -c $(CFLAGS) $(CryptoLibSRC)

crackerlib: $(CrackerLibObj) $(CryptoLibObj)
	ar rcs lib$(CrackerLib).a $(CrackerLibObj) $(CryptoLibObj)

$(CrackerLibObj): $(CrackLibSRC)
	gcc -c $(CFLAGS) $(CrackLibSRC)

