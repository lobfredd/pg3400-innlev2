void initSocket(char* serverAddress, void (*rListener)(char* msg));
void sendMsg(char* msg);
void closeSocket();