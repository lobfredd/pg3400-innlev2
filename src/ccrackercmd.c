#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <stdbool.h>
#include "headers/cracker.h"
#include "headers/stringutil.h"
#include "headers/charlist.h"
#include "headers/staticStrings.h"

char* readFile(char* path){
	CharList* string = initCharList(100);
	FILE *f = fopen(path, "r");
	char c;
	while((c = fgetc(f)) != EOF) {
		string->add(string, c);
    }
    fclose(f);
    return detachCharArrayAndFree(string);
}

void printProgress(int percent){
	percent++; //ending at 100%
	printf("%s:\t%3d%%[", "Cracking", percent);
	for(int i = 0; i < percent/5; i++){
		printf("#");
	}
	for(int i = 0; i < 20-(percent/5); i++){
		printf(" ");
	}
	printf("]\n");
}

void progressStatus(double v){
	static bool alreadyPrinted = false;
	if(alreadyPrinted){
		printf("\033[F\033[J"); //Remove last output line
		printProgress(v);
	}
	else{
		printProgress(v);
		alreadyPrinted = true;
	}
}

void printHelp(){
	printf(CCRACKERCMD_HELP);
}

int main (int argc, char* argv[]) {
	char action = getopt(argc, argv, "h");
	switch(action){
		case 'h':
			printHelp();
			exit(0);
	}
	if(argc < 3 || access(argv[1], F_OK) == -1 || access(argv[2], F_OK) == -1){
		printHelp();
		exit(1); //not enough/invalid args, quiting
	} 

	
	char* targetMsg = readFile(argv[1]);
	char* keyFolderPath = argv[2];
	
	char* result = crack(targetMsg, keyFolderPath, progressStatus);
	free(targetMsg);

	printf("Key: %s\n", (result != NULL) ? result : "No match..");
	free(result);
	
	return 0;
}   
  