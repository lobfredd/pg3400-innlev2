//http://stackoverflow.com/a/16973237
//sudo apt-get install libgtk-3-dev
#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <bits/sigthread.h>
#include "headers/cracker.h"
#include "headers/gtkutils.h"
#include "headers/stringutil.h"

GtkWidget *window;
pthread_t crackingThread;
ProgressBarDialog pbd;
ScrollableTextView keyPath;
ScrollableTextView target;
ScrollableTextView result;

char* keyFolderPath;

void askForKeyFolderPath(){
	keyFolderPath = openFileDialog("Open Folder", GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER, window);
	if(keyFolderPath != NULL){
		gtk_text_buffer_set_text(keyPath.textBuffer, keyFolderPath, -1);
	}
}

gboolean updateProgressBar(gpointer val){
	double value = *((double*)val);
	gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(pbd.progressBar), value);
	free(val);
	return FALSE;
}

gboolean destroyWidget(gpointer widget){
	gtk_widget_destroy(widget);
	return FALSE;
}
gboolean updateResult(gpointer res){
	if(res == NULL) gtk_text_buffer_set_text(result.textBuffer, "No match...", -1);
	else{
		gtk_text_buffer_set_text(result.textBuffer, res, -1);
		free(res);
	} 
	return FALSE;
}

void askGuiThreadToUpdateProgressbar(double value){
	value /= 100.0; //progressBar Widget uses 0-1 value
	double* v = malloc(sizeof(double)); //because threads
	if(v == NULL){
		fprintf(stderr, "Out of memory?");
		exit(1);
	}
	*v = value;
	gdk_threads_add_idle(updateProgressBar, v);
}

void* doCrack(void* attr){
	char* res = crack(attr, keyFolderPath, askGuiThreadToUpdateProgressbar);
	g_free(attr);
	gdk_threads_add_idle(updateResult, res);
	gdk_threads_add_idle(destroyWidget, pbd.dialog);
	return NULL;
}

void startAsyncCracking(){
	if(keyFolderPath == NULL) return;
	char* encoded = getAllTextFromBuffer(target.textBuffer);
	if(strlen(encoded) < 1) return;
	pbd = createAndshowProgressBar("Cracking...", window);
	pthread_create(&crackingThread, NULL, doCrack, encoded);
}

int main(int argc, char *argv[]){
	GtkWidget *grid;
	GtkWidget *button;
	GtkWidget* title;

	gtk_init(&argc, &argv);

	//Main window
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_name(window, "mainWindow");
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), 380, 400);
	gtk_window_set_title(GTK_WINDOW(window), "cCracker");
	gtk_container_set_border_width(GTK_CONTAINER(window), 15);

	//Grid
	grid = gtk_grid_new();
	gtk_grid_set_row_spacing(GTK_GRID(grid), 10);
	gtk_grid_set_column_spacing(GTK_GRID(grid), 10);
	gtk_container_add(GTK_CONTAINER(window), grid);

	//Key Path Label
	keyPath.textView = gtk_text_view_new();
	keyPath.textBuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(keyPath.textView));
	gtk_widget_set_name(keyPath.textView, "keyPathTextView");
	gtk_text_view_set_editable(GTK_TEXT_VIEW(keyPath.textView), FALSE);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(keyPath.textView), FALSE);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(keyPath.textView), GTK_WRAP_WORD_CHAR);
	gtk_grid_attach(GTK_GRID(grid), keyPath.textView, 0, 0, 6, 1);
	gtk_text_buffer_set_text(keyPath.textBuffer, "Choose key folder...", -1);

	//Choose button
	button = createButton("Choose");
	gtk_widget_set_size_request(button, 80, 40);
	gtk_widget_set_halign(button, GTK_ALIGN_END);
	gtk_widget_set_valign(button, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), button, 6, 0, 2, 1);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(askForKeyFolderPath), NULL);

	//Title
	title = gtk_label_new("Target text:");
	gtk_widget_set_halign(title, GTK_ALIGN_START);
	gtk_widget_set_valign(title, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), title, 0, 1, 8, 1);

	//Target text view
	target = createScrollableTextView();
	gtk_grid_attach(GTK_GRID(grid), target.scrollContainer, 0, 2, 8, 4);

	button = createButton("Crack");
	gtk_widget_set_size_request(button, 160, 40);
	gtk_widget_set_halign(button, GTK_ALIGN_CENTER);
	gtk_grid_attach(GTK_GRID(grid), button, 0, 6, 8, 1);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(startAsyncCracking), NULL);

	//Title
	title = gtk_label_new("Result:");
	gtk_widget_set_halign(title, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), title, 0, 7, 2, 1);

	//Result text view
	result = createScrollableTextView();
	gtk_text_view_set_editable(GTK_TEXT_VIEW(result.textView), FALSE);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(result.textView), FALSE);
	gtk_grid_attach(GTK_GRID(grid), result.scrollContainer, 0, 8, 8, 1);

	g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
	loadCss();
	gtk_widget_show_all(window);
	gtk_main();

	return 0;
}