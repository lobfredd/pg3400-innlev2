#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../headers/charlist.h"

void resizeCharArray(CharList *self){
	self->length *= 2;
	self->array = realloc(self->array, self->length * sizeof(char));
	if(self->array == NULL){
		fprintf(stderr, "Out of memory?");
		exit(1);
	}
}

static int spaceLeft(CharList *self){
	return self->length - self->count;
}

void addChar(CharList *self, char c){
	if(!spaceLeft(self)) resizeCharArray(self);
	self->array[self->count++] = c;
}

void addIntAsString(CharList *self, char* format, int num){
	while(snprintf(NULL, 0, format, num) >= self->spaceLeft(self))
		resizeCharArray(self);

	self->count += snprintf(&self->array[self->count], MAX_INT_STRING_LENGTH, format, num);
}

CharList* initCharList(int length){
	CharList *cList = malloc(sizeof(CharList));
	if(cList == NULL){
		fprintf(stderr, "Out of memory?");
		exit(1);
	}

	if(length < 1) length = 1;
	cList->length = length;
	cList->array = malloc(cList->length * sizeof(char));
	if(cList->array == NULL){
		fprintf(stderr, "Out of memory?");
		exit(1);
	}
	cList->count = 0;
	cList->add = addChar;
	cList->spaceLeft = spaceLeft;
	cList->addIntAsString = addIntAsString;
	return cList;
}

void freeCharList(CharList *self){
	free(self->array);
	free(self);
}

char *detachCharArrayAndFree(CharList *self){
	self->add(self, '\0');
	char* array = malloc(self->count * sizeof(char));
	if(array == NULL){
		fprintf(stderr, "Out of memory?");
		exit(1);
	}
	memcpy(array, self->array, self->count);
	freeCharList(self);
	return array;
}
