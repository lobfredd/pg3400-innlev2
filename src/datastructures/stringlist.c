#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../headers/stringlist.h"

void resizeStringArray(StringList *self){
	self->length *= 2;
	self->array = realloc(self->array, self->length * sizeof(char*));
	if(self->array == NULL){
		fprintf(stderr, "Out of memory?");
		exit(1);
	}
}

static int spaceLeft(StringList *self){
	return self->length - self->count;
}

void addString(StringList *self, char* s){
	if(!spaceLeft(self)) resizeStringArray(self);
	self->array[self->count++] = s;
}

StringList* initStringList(int length){
	StringList *sList = malloc(sizeof(StringList));
	if(sList == NULL){
		fprintf(stderr, "Out of memory?");
		exit(1);
	}

	if(length < 1) length = 1;
	sList->length = length;
	sList->array = malloc(sList->length * sizeof(char*));
	if(sList->array == NULL){
		fprintf(stderr, "Out of memory?");
		exit(1);
	}

	sList->count = 0;
	sList->add = addString;
	sList->spaceLeft = spaceLeft;
	return sList;
}

void freeStringList(StringList *self){
	for(int i = 0; i < self->count; i++) free(self->array[i]);
	free(self->array);
	free(self);
}
