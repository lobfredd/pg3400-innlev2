//http://stackoverflow.com/a/16973237
//sudo apt-get install libgtk-3-dev
#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <bits/sigthread.h>
#include "headers/ccrypto.h"
#include "headers/gtkutils.h"

GtkWidget *window;
ProgressBarDialog pbd;
ScrollableTextView keyPathView;
ScrollableTextView targetView;
ScrollableTextView resultView;
char* key = NULL;
pthread_t readFileThread;

void* readFile(void *attr){
	char* path = (char*) attr;
	key = makeKeyFromFile(path);
	g_free(path);
	return NULL;
}

gboolean pulsateProgressBar(gpointer attr){
	gtk_progress_bar_pulse(GTK_PROGRESS_BAR(pbd.progressBar));
	gboolean running = pthread_kill(readFileThread, 0) == 0; //is thread still running?
	if(!running) gtk_widget_destroy(pbd.dialog);
	return running;
}

void readFileAsync(char* path){
	pbd = createAndshowProgressBar("Reading file...", window);
	gtk_progress_bar_set_show_text(GTK_PROGRESS_BAR(pbd.progressBar), FALSE);
	if(key != NULL) free(key);
	pthread_create(&readFileThread, NULL, readFile, path);
	g_timeout_add(100, pulsateProgressBar, NULL);
}

void askForKeyFilePath(){
	char* keyPath = openFileDialog("Open File", GTK_FILE_CHOOSER_ACTION_OPEN, window);
	if(keyPath != NULL){
		readFileAsync(keyPath);
		gtk_text_buffer_set_text(keyPathView.textBuffer, keyPath, -1);
	}
}

void encodeDecode(char* (*action)(const char*, const char*, StatusCode *)){
	if(key == NULL){
		showMessageDialog(window, GTK_MESSAGE_ERROR, getStatusMsg(KeyFileError));
		return;
	}
	StatusCode s;
	char* txt = getAllTextFromBuffer(targetView.textBuffer);
	if(strlen(txt) < 1){
		g_free(txt);
		return;
	}
	char* result = action(txt, key, &s);

	if(s == InvalidKey) showMessageDialog(window, GTK_MESSAGE_ERROR, getStatusMsg(s));
	else{
		if(s == DConditionNotMet) showMessageDialog(window, GTK_MESSAGE_WARNING, getStatusMsg(s));
		gtk_text_buffer_set_text(resultView.textBuffer, result, -1);
	}
	g_free(txt);
	free(result);
}

void doEncode(){
	encodeDecode(encode);
}

void doDecode(){
	encodeDecode(decode);
}

int main(int argc, char *argv[]){
	GtkWidget *grid;
	GtkWidget *button;
	GtkWidget* title;

	gtk_init(&argc, &argv);

	//Main window
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_name(window, "mainWindow");
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), 480, 700);
	gtk_window_set_title(GTK_WINDOW(window), "cCipher");
	gtk_container_set_border_width(GTK_CONTAINER(window), 15);

	//Grid
	grid = gtk_grid_new();
	gtk_grid_set_row_spacing(GTK_GRID(grid), 10);
	gtk_grid_set_column_spacing(GTK_GRID(grid), 10);
	gtk_container_add(GTK_CONTAINER(window), grid);

	//Key Path Label
	keyPathView.textView = gtk_text_view_new();
	keyPathView.textBuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(keyPathView.textView));
	gtk_widget_set_name(keyPathView.textView, "keyPathTextView");
	gtk_text_view_set_editable(GTK_TEXT_VIEW(keyPathView.textView), FALSE);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(keyPathView.textView), FALSE);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(keyPathView.textView), GTK_WRAP_WORD_CHAR);
	gtk_grid_attach(GTK_GRID(grid), keyPathView.textView, 0, 0, 6, 1);
	gtk_text_buffer_set_text(keyPathView.textBuffer, "Choose key file...", -1);

	//Choose button
	button = createButton("Choose");
	gtk_widget_set_size_request(button, 80, 40);
	gtk_widget_set_halign(button, GTK_ALIGN_END);
	gtk_widget_set_valign(button, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), button, 6, 0, 2, 1);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(askForKeyFilePath), NULL);

	//Title
	title = gtk_label_new("Target text:");
	gtk_widget_set_halign(title, GTK_ALIGN_START);
	gtk_widget_set_valign(title, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), title, 0, 1, 8, 1);

	//Target text view
	targetView = createScrollableTextView();
	gtk_grid_attach(GTK_GRID(grid), targetView.scrollContainer, 0, 2, 8, 4);

	//Encode button
	button = createButton("Encode");
	gtk_widget_set_size_request(button, 160, 40);
	gtk_widget_set_halign(button, GTK_ALIGN_END);
	gtk_grid_attach(GTK_GRID(grid), button, 1, 6, 2, 1);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(doEncode), NULL);

	//Decode Button
	button = createButton("Decode");
	gtk_widget_set_size_request(button, 160, 40);
	gtk_widget_set_halign(button, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), button, 5, 6, 2, 1);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(doDecode), NULL);

	//Title
	title = gtk_label_new("Result text:");
	gtk_widget_set_halign(title, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), title, 0, 7, 8, 1);

	//Result text view
	resultView = createScrollableTextView();
	gtk_grid_attach(GTK_GRID(grid), resultView.scrollContainer, 0, 8, 8, 4);

	g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
	loadCss();
	gtk_widget_show_all(window);
	gtk_main();
	free(key);

	return 0;
}