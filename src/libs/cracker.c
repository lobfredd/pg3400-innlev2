#include <stdlib.h>
#include <stdio.h>
#include <dirent.h> 
#include <sys/stat.h>
#include "../headers/extendedccrypto.h"
#include "../headers/stringutil.h"
#include "../headers/intlist.h"
#include "../headers/charlist.h"
#include "../headers/stringlist.h"
// sudo apt-get install libssl-dev

StringList* dict;
StringList* keys;

bool matchWord(CharList* decodedWord){
	if(decodedWord->count < 2) return false;
	for(int i = 0; i < dict->count; i++){
		if(strcmp(dict->array[i], decodedWord->array) == 0)
			return true;
	}
	return false;
}

char* extractNextWord(char* encoded, char* key, CharList* decodedWord){
	int keyLength = strlen(key);
	int encodedLength = strlen(encoded);
	int i = 0;
	for(; i < encodedLength && encoded[i] != '['; i++);
	for(; i < encodedLength; i++){
		char* cipher = getCipher(&encoded[i], encodedLength);
		int cipherLength = (cipher != NULL) ? strlen(cipher) : 0;
		if(cipherLength > 0){
			int cipherAsInt;
			if(validIntExtracted(cipher, cipherLength, &cipherAsInt)){
				if(keyLength < abs(cipherAsInt)-1){
					free(cipher);
					return NULL;
				} 
				decodedWord->add(decodedWord, key[abs(cipherAsInt)]);
			}
			i += cipherLength+1;
			free(cipher);
		}
		else break;
	}
	decodedWord->add(decodedWord, '\0');
	return &encoded[i];
}

bool correctKey(char* encoded, char* key){
	for(int i = 0; i < 3; i++){
		int encodedLength = strlen(encoded);
		if(encodedLength < 3) break;
		CharList* decodedWord = initCharList(encodedLength);
		encoded = extractNextWord(encoded, key, decodedWord);
		bool isWord = (encoded == NULL) ? false : matchWord(decodedWord);
		freeCharList(decodedWord);
		if(!isWord) return false;
	}
	return true;
}

// http://stackoverflow.com/questions/4553012/checking-if-a-file-is-a-directory-or-just-a-file
bool isFile(const char *path){
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISREG(path_stat.st_mode);
}

char* foldersNeedSlashAtEnd(char* path){
	if(path[strlen(path)-1] != '/'){
		return concat(path, "/");
	}
	else return concat(path, ""); //making a copy anyway, not messing with callers string
}

void readFileNamesFrom(char* keysFolderPath){
	char* folderPath = foldersNeedSlashAtEnd(keysFolderPath);
	DIR *d;
  	struct dirent *dir;
  	d = opendir(folderPath);
  	if(d){
    	while((dir = readdir(d)) != NULL){
    		char* path = concat(folderPath, dir->d_name);
    		if(isFile(path)) keys->add(keys, path);
    		else if(strcmp(dir->d_name, "..") == 0 || strcmp(dir->d_name, ".") == 0) free(path);
    		else{
    			readFileNamesFrom(path);
    			free(path);
    		} 
    	}
    	closedir(d);
  	}
  	free(folderPath);
}

void readDict(){
	dict = initStringList(100000);
	FILE *f = fopen("/usr/share/dict/words", "r");
	do{
		CharList* word = initCharList(50);
		char c;
		while((c = fgetc(f)) != EOF && c != '\n'){ //char for char makes a word, assumes one word per line tho
			word->add(word, c);
		}

		if(word->count > 0){
			char* string = detachCharArrayAndFree(word);
			dict->add(dict, string);
		}
		else freeCharList(word);
		
	}while(!feof(f));
	fclose(f);
}

void reportProgress(int i, int totalTimes, void(*updateProgress)(double v)){
	static int last = 0;
	int percent = (i / (double)totalTimes * 100.0);
		if(last != percent){
			last = percent;
			updateProgress(percent);
		}

}

char* crack(char* encoded, char* keysFolderPath, void(*updateProgress)(double v)){
	readDict();
	keys = initStringList(10);
	readFileNamesFrom(keysFolderPath);
	char* result = NULL;

	for(int i = 0; i < keys->count; i++){
		reportProgress(i, keys->count, updateProgress);

		char* key = makeKeyFromFile(keys->array[i]);
		if(correctKey(encoded, key)){
			result = concat(keys->array[i], ""); //just for copy
			free(key);
			break;
		}
		free(key);
	}

	freeStringList(dict);
	freeStringList(keys);
	
	return result;
}
