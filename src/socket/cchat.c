//http://stackoverflow.com/a/16973237
//sudo apt-get install libgtk-3-dev
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <bits/sigthread.h>
#include "../headers/ccrypto.h"
#include "../headers/gtkutils.h"
#include "../headers/client.h"

GtkWidget *window;
ProgressBarDialog pbd;
ScrollableTextView serverAddressView;
ScrollableTextView keyPathView;
ScrollableTextView usernameView;
ScrollableTextView chatView;
ScrollableTextView messageView;
char* key = NULL;
char* serverAddress = NULL;
char* username = NULL;
pthread_t readFileThread;

char* encodeDecode(char* targetText, char* (*action)(const char*, const char*, StatusCode *)){
	StatusCode s;
	char* result = action(targetText, key, &s);
	if(s == InvalidKey) showMessageDialog(window, GTK_MESSAGE_ERROR, getStatusMsg(s));
	else if(s == DConditionNotMet) showMessageDialog(window, GTK_MESSAGE_WARNING, getStatusMsg(s));
	return result;
}

void freeKeyFile(){
	free(key);
	key = NULL;
	gtk_text_buffer_set_text(keyPathView.textBuffer, "Choose key file...", -1);
}

void* readFile(void *attr){
	char* path = (char*) attr;
	key = makeKeyFromFile(path);
	if(key == NULL){
		showMessageDialog(window, GTK_MESSAGE_ERROR, getStatusMsg(KeyFileError));
		freeKeyFile();
	} 
	g_free(path);
	return NULL;
}

gboolean pulsateProgressBar(gpointer attr){
	gtk_progress_bar_pulse(GTK_PROGRESS_BAR(pbd.progressBar));
	gboolean running = pthread_kill(readFileThread, 0) == 0; //is thread still running?
	if(!running) gtk_widget_destroy(pbd.dialog);
	return running;
}

void readFileAsync(char* path){
	pbd = createAndshowProgressBar("Reading file...", window);
	gtk_progress_bar_set_show_text(GTK_PROGRESS_BAR(pbd.progressBar), FALSE);
	if(key != NULL) free(key);
	pthread_create(&readFileThread, NULL, readFile, path);
	g_timeout_add(100, pulsateProgressBar, NULL);
}

void askForKeyFilePath(){
	char* keyPath = openFileDialog("Open File", GTK_FILE_CHOOSER_ACTION_OPEN, window);
	if(keyPath != NULL){
		readFileAsync(keyPath);
		gtk_text_buffer_set_text(keyPathView.textBuffer, keyPath, -1);
	}
}

void sendMessageG(){
	char* msg = getAllTextFromBuffer(messageView.textBuffer);
	if(strlen(msg) < 1){
		g_free(msg);
		return;
	}
	
	if(key != NULL){
		char* encoded = encodeDecode(msg, encode);
		if (encoded != NULL){
			sendMsg(encoded);
			gtk_text_buffer_set_text(messageView.textBuffer, "", -1);
		} 
		free(encoded);
	} 
	else{
		sendMsg(msg);
		gtk_text_buffer_set_text(messageView.textBuffer, "", -1);
	} 
	g_free(msg);
}

gboolean updateRecieved(gpointer msg){
	char* decoded = NULL;
	if(key != NULL){
		decoded = encodeDecode(msg, decode);
	}
	if(decoded != NULL) appendTextToBuffer(chatView, decoded);
	else appendTextToBuffer(chatView, msg);
	appendTextToBuffer(chatView, "\n");
	free(decoded);
	free(msg);
	return FALSE;
}

void reciever(char* msg){
	char* message = malloc((strlen(msg)+1) * sizeof(char));
	memcpy(message, msg, strlen(msg)+1);
	gdk_threads_add_idle(updateRecieved, message);
}

void setUsername(){
	g_free(username);
	username = getAllTextFromBuffer(usernameView.textBuffer);
	closeSocket();
	initSocket(serverAddress, reciever);
	sendMsg(username);
}

void setServerAddress(){
	g_free(serverAddress);
	serverAddress = getAllTextFromBuffer(serverAddressView.textBuffer);
	setUsername();
}


gpointer onEnterPress(GtkWidget *widget, GdkEventKey *event, gpointer user_data){
	if(event->keyval != GDK_KEY_Return) return FALSE;
	sendMessageG();
	return (void*)1; //random not NULL to avoid enter being registered as newline
}

int main(int argc, char *argv[]){
	GtkWidget *grid;
	GtkWidget *button;
	GtkWidget* title;

	gtk_init(&argc, &argv);

	//Main window
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_name(window, "mainWindow");
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), 380, 400);
	gtk_window_set_title(GTK_WINDOW(window), "cChat");
	gtk_container_set_border_width(GTK_CONTAINER(window), 15);

	//Grid
	grid = gtk_grid_new();
	gtk_grid_set_row_spacing(GTK_GRID(grid), 10);
	gtk_grid_set_column_spacing(GTK_GRID(grid), 10);
	gtk_container_add(GTK_CONTAINER(window), grid);

	///URL text view
	serverAddressView = createScrollableTextView();
	gtk_grid_attach(GTK_GRID(grid), serverAddressView.scrollContainer, 0, 0, 7, 1);
	gtk_widget_set_valign(serverAddressView.scrollContainer, GTK_ALIGN_START);
	gtk_text_buffer_set_text(serverAddressView.textBuffer, "62.16.208.49", -1);
	gtk_widget_set_vexpand(serverAddressView.scrollContainer, FALSE);

	//Set URL button
	button = createButton("Set URL");
	gtk_widget_set_size_request(button, 80, 40);
	gtk_widget_set_halign(button, GTK_ALIGN_END);
	gtk_widget_set_valign(button, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), button, 6, 0, 2, 1);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(setServerAddress), NULL);

	//Key Path Label
	keyPathView.textView = gtk_text_view_new();
	keyPathView.textBuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(keyPathView.textView));
	gtk_widget_set_name(keyPathView.textView, "keyPathTextView");
	gtk_text_view_set_editable(GTK_TEXT_VIEW(keyPathView.textView), FALSE);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(keyPathView.textView), FALSE);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(keyPathView.textView), GTK_WRAP_WORD_CHAR);
	gtk_grid_attach(GTK_GRID(grid), keyPathView.textView, 0, 1, 5, 1);
	gtk_text_buffer_set_text(keyPathView.textBuffer, "Choose key file...", -1);

	//X button
	button = createButton("X");
	gtk_widget_set_size_request(button, 40, 40);
	gtk_widget_set_halign(button, GTK_ALIGN_END);
	gtk_widget_set_valign(button, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), button, 6, 1, 1, 1);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(freeKeyFile), NULL);

	//Choose button
	button = createButton("Choose");
	gtk_widget_set_size_request(button, 80, 40);
	gtk_widget_set_halign(button, GTK_ALIGN_END);
	gtk_widget_set_valign(button, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), button, 6, 1, 2, 1);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(askForKeyFilePath), NULL);

	//username text view
	usernameView = createScrollableTextView();
	gtk_grid_attach(GTK_GRID(grid), usernameView.scrollContainer, 0, 2, 7, 1);
	gtk_text_buffer_set_text(usernameView.textBuffer, "Unknown Soldier", -1);
	gtk_widget_set_vexpand(usernameView.scrollContainer, FALSE);

	//set username button
	button = createButton("Set");
	gtk_widget_set_size_request(button, 80, 40);
	gtk_widget_set_halign(button, GTK_ALIGN_END);
	gtk_widget_set_valign(button, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), button, 6, 2, 2, 1);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(setUsername), NULL);

	//Title
	title = gtk_label_new("The Chat");
	gtk_widget_set_halign(title, GTK_ALIGN_START);
	gtk_widget_set_valign(title, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), title, 0, 3, 8, 1);

	//Target text view
	chatView = createScrollableTextView();
	gtk_text_view_set_editable(GTK_TEXT_VIEW(chatView.textView), FALSE);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(chatView.textView), FALSE);
	gtk_grid_attach(GTK_GRID(grid), chatView.scrollContainer, 0, 4, 8, 6);

	//Title
	title = gtk_label_new("Write somethin..");
	gtk_widget_set_halign(title, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(grid), title, 0, 10, 8, 1);

	//Message text view
	messageView = createScrollableTextView();
	gtk_grid_attach(GTK_GRID(grid), messageView.scrollContainer, 0, 11, 7, 2);
	g_signal_connect(G_OBJECT(messageView.textView), "key_press_event", G_CALLBACK(onEnterPress), NULL);

	button = createButton("Send");
	gtk_widget_set_size_request(button, 80, 40);
	gtk_widget_set_halign(button, GTK_ALIGN_CENTER);
	gtk_widget_set_valign(button, GTK_ALIGN_END);
	gtk_grid_attach(GTK_GRID(grid), button, 7, 11, 1, 1);
	g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(sendMessageG), NULL);

	serverAddress = getAllTextFromBuffer(serverAddressView.textBuffer);
	username = getAllTextFromBuffer(usernameView.textBuffer);
	initSocket(serverAddress, reciever);
	sendMsg(username);

	g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
	loadCss();
	gtk_widget_show_all(window);
	gtk_main();

	return 0;
}