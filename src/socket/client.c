#define _BSD_SOURCE
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <arpa/inet.h>
#include "../headers/extendedccrypto.h"
#include "../headers/charlist.h"
#include "../headers/socketutil.h"
#include "../headers/staticStrings.h"

int serverSocket;
void (*recieveListener)(char* msg);
pthread_t threadID;


void sendMsg(char* msg) {
	sendMessage(serverSocket, msg);
}

void* getMessageThread(void* attr) {
	pthread_setcanceltype(3, 0);
	char* msg = NULL;
	while (true) {
		int status;
		msg = receiveMessage(serverSocket, &status);
		if(status == CLIENT_MESSAGE_ERROR) {	//TA BORT CLIENT
			perror(MESSAGE_ERROR);
			break;
		} else if (status == CLIENT_LEFT) {	//TA BORT CLIENT
			printf(SERVER_SHUT_DOWN);
			break;
		} else {
			recieveListener(msg);
			free(msg);
			msg = NULL;
		}	
	}
	free(msg);
	pthread_exit(NULL);
}

void createThread() {
	pthread_create(&threadID, NULL, getMessageThread, NULL);
}

void reportErrorCloseSocket(char* errorMsg){
	perror(errorMsg);
	close(serverSocket);
}

struct hostent* setupHostDetails(char* addrString){
	struct in_addr addr;
	struct hostent *host;
	if(inet_aton(addrString, &addr) == 0) host = gethostbyname(addrString);
	else host = gethostbyaddr(&addr.s_addr, sizeof(int), AF_INET);
	return host;
}

struct sockaddr_in setupServer(struct hostent* host){
	struct sockaddr_in server;
	server.sin_family = AF_INET;
	memcpy(&server.sin_addr, host->h_addr_list[0], host->h_length);
	server.sin_port = htons(5000);
	return server;
}


void closeSocket(){
	close(serverSocket);
	pthread_cancel(threadID);
	pthread_join(threadID, NULL);
	recieveListener = NULL;
}

void initSocket(char* serverAddress, void (*rListener)(char* msg)){
	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (serverSocket < 0) return reportErrorCloseSocket(CREATE_SOCKET_ERROR);

	struct hostent *host = setupHostDetails(serverAddress);
	if (host == NULL) return reportErrorCloseSocket(WRONG_HOST_ERROR);

	struct sockaddr_in server = setupServer(host);

	if(connect(serverSocket, (struct sockaddr *) &server, sizeof(server)) >= 0) {
		recieveListener = rListener;
		createThread();
	} 
	else reportErrorCloseSocket(SERVER_CONNECT_ERROR);
}
