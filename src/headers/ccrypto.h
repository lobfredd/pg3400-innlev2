#ifndef CCRYPTO
#define CCRYPTO

#define SUCCESS_MSG "Operation completed successfully!" //typo?
#define MSG_FILE_ERROR_MSG "Unable to read the message file!" //typo?
#define KEY_FILE_ERROR_MSG "Unable to read the key file!" //typo?
#define D_CONDITION_ERROR_MSG "The D condition could not be met" //typo?
#define INVALID_KEY_ERROR_MSG "The specified key is invalid, does it contain the whole alphabet?" //typo?

typedef enum{
	Success,
	MsgFileError,
	KeyFileError,
	DConditionNotMet,
	InvalidKey
}StatusCode;


void printStatus(StatusCode s);
char* getStatusMsg(StatusCode s);
void setD(int distance);
char* makeKeyFromFile(char *filename);

/* Some documentation regarding inputs to the encode function and what to expect as
* output. Note that I need two outputs from the encoder - One is the encoded
* stream, the other is the status if success or fail. Hence, I can use two
* different forms. I am placing both the forms here.
*
*/
char* encode(const char *msg, const char *key, StatusCode *status);
// This form returns char * array of encoded stream and status into the
// reference.

//Do not alloc encoded.. == mem-leak
StatusCode sencode(const char *msg, const char *key, char **encoded);
// This form returns the status and takes the reference to the encoded stream as
// an argument
/* Some documentation regarding decode function and what to expect as output.
* This functions pretty much the same as encode function
*/
char *decode(const char *encoded, const char *key, StatusCode *status);

//Do not alloc decoded.. == mem-leak
StatusCode sdecode(const char *encoded, const char *key, char **decoded);


#endif