#ifndef EXTCCRYPTO
#define EXTCCRYPTO
#include <stdbool.h>
#include "ccrypto.h"

bool validIntExtracted(char* str, int length, int* dest);
char* getCipher(const char* encoded, int length);

#endif