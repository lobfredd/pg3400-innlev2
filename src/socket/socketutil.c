#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "../headers/extendedccrypto.h"
#include "../headers/charlist.h"
#include "../headers/socketutil.h"

void sendMessage(int socket, char* message){
	int length = strlen(message);
	if(length > 1500) return;
	char* lengthAsString = calloc(MAX_INT_STRING_LENGTH+1, sizeof(char));
	snprintf(lengthAsString, MAX_INT_STRING_LENGTH, "%d", length);
	
	send(socket, lengthAsString, MAX_INT_STRING_LENGTH, MSG_NOSIGNAL);
	free(lengthAsString);
	send(socket, message, strlen(message), MSG_NOSIGNAL);
}

char* receiveMessage(int socket, int* status){
	char* lengthAsString = calloc(MAX_INT_STRING_LENGTH+1, sizeof(char));
	*status = recv(socket, lengthAsString, MAX_INT_STRING_LENGTH, 0);
	int length;
	char* msg = NULL;
	if(validIntExtracted(lengthAsString, strlen(lengthAsString), &length)){
		msg = calloc(length+1, sizeof(char));
		*status = recv(socket, msg, length, 0);
	} 
	free(lengthAsString);
	return msg;
}