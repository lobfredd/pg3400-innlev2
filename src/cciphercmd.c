#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include "headers/extendedccrypto.h"
#include "headers/stringutil.h"
#include "headers/charlist.h"
#include "headers/staticStrings.h"

char* readFile(char* path){
	CharList* string = initCharList(100);
	FILE *f = fopen(path, "r");
	char c;
	while((c = fgetc(f)) != EOF) {
		string->add(string, c);
    }
    fclose(f);
    return detachCharArrayAndFree(string);
}

int main (int argc, char* argv[]) {
	char* (*theAction)(const char* msg, const char* key, StatusCode *status);
	char action = getopt(argc, argv, "edh");
	switch(action){
		case 'e':
			theAction = encode;
			break;
		case 'd':
			theAction = decode;
			break;
		case 'h':
			printf(CCIPHERCMD_HELP);
			exit(0);
		default:
			exit(1);
	}
	if(argc < 4 || access(argv[2], F_OK) == -1 || access(argv[3], F_OK) == -1) exit(1); //not enough/invalid args, quiting
	int d = -1;
	if(argc > 4 && validIntExtracted(argv[4], strlen(argv[4]), &d)) setD(d); //setting d if supplied
	
	char* targetmsg = readFile(argv[2]);
	char* keyPath = argv[3];
	char* key = makeKeyFromFile(keyPath);
	StatusCode s;
	
	char* result = theAction(targetmsg, key, &s);
	free(key);
	free(targetmsg);
	if(s != Success) printStatus(s);
	else printf("%s\n", result);
	free(result);
	
	return 0;
}   
  