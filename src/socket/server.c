#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <unistd.h>
#include "../headers/intlist.h"
#include "../headers/charlist.h"
#include "../headers/stringutil.h"
#include "../headers/extendedccrypto.h"
#include "../headers/socketutil.h"
#include "../headers/staticStrings.h"

IntList* clients;
pthread_mutex_t lock;

void setSocketAttributes(struct sockaddr_in* server) {
	server->sin_family = AF_INET;
	server->sin_addr.s_addr = INADDR_ANY;
	server->sin_port = htons(5000);
}

bool createSocket(int* sock) {
	*sock = socket(AF_INET, SOCK_STREAM, 0);
	return (*sock > 0);
}

void broadcastMessage(char* message) {
	pthread_mutex_lock(&lock);
	for (int i = 0; i < clients->count; i++){
		sendMessage(clients->array[i], message);
	}
	pthread_mutex_unlock(&lock);
}

void conversation(int incomingCon, char* username){
	char* joinedMessage = concat(username, JOINED);
	broadcastMessage(joinedMessage);
	printf("%s\n", joinedMessage);
	free(joinedMessage);

	int status;
	bool conActive = true;
	while(conActive) {
		char* msg = receiveMessage(incomingCon, &status);
		char* broadcastMsg;
		if (status == CLIENT_LEFT) {
			broadcastMsg = concat(username, LEFT);
			printf("%s\n", broadcastMsg);

			pthread_mutex_lock(&lock);
			clients->remove(clients, incomingCon);
			pthread_mutex_unlock(&lock);
			conActive = false;
		}
		else {
			char* usernameWithColon = concat(username, ": ");
			broadcastMsg = concat(usernameWithColon, msg);
			free(usernameWithColon);
		}
		broadcastMessage(broadcastMsg);
		free(broadcastMsg);
		free(msg);
	}
}

void* handleCon(void* attr) {
	int incomingCon = *((int*)attr);
	free(attr);

	int status;
	char* username = receiveMessage(incomingCon, &status);
	if(status > 0){
		conversation(incomingCon, username);
	} 
	else if(status == CLIENT_MESSAGE_ERROR) {
		perror(MESSAGE_ERROR);
	}

	free(username);
	close(incomingCon);
	pthread_exit(NULL);
}

void createThread(int incomingCon) {
	int* con = malloc(sizeof(int)); //because threads
	*con = incomingCon;
	pthread_t threadID;
	pthread_create(&threadID, NULL, handleCon, con);
}

void connectionListener(int* socket) {
	int incomingCon;
	while (true) {
		incomingCon = accept(*socket, (struct sockaddr *) 0, 0);
		if (incomingCon == -1) {
			perror(ACCEPT_FAIL);
		} 
		else {
			pthread_mutex_lock(&lock);
			clients->add(clients, incomingCon);
			pthread_mutex_unlock(&lock);

			createThread(incomingCon);
		}
	} 
}

int main(int argc, char *argv[]) {
	int socket;
	struct sockaddr_in server;
	clients = initIntList(100);
	pthread_mutex_init(&lock, NULL);

	if(createSocket(&socket)) {
		setSocketAttributes(&server);
		if(bind(socket, (struct sockaddr *)&server, sizeof(server)) == 0) {
			listen(socket, 5);
			connectionListener(&socket);
		} else perror(BIND_FAIL);
	} else perror(CREATE_SOCKET_ERROR);
	

	freeIntList(clients);
	close(socket);
	pthread_mutex_destroy(&lock);
	return 0;
}