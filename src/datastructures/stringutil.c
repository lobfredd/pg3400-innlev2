#include "../headers/stringutil.h"

char lowercase(char letter) {
	return letter | 0x20;
}

char uppercase(char letter){
	return letter & ~(0x20);
}

bool isUpperCaseChar(char c){
	return (c >= 'A' && c <= 'Z');
}

bool isLowerCaseChar(char c){
	return (c >= 'a' && c <= 'z');
}

bool isASCIILetter(char c){
	return isUpperCaseChar(c) || isLowerCaseChar(c);
}

bool isASCIINumber(char c){
	return (c >= '0' && c <= '9') || (c == '-' || c == '+');
}

//http://stackoverflow.com/a/8465083
char* concat(char *s1, char *s2){
    char *result = calloc(strlen(s1)+strlen(s2)+1, sizeof(char));//+1 for the zero-terminator
    if(result == NULL){
    	fprintf(stderr, "Out of memory?");
    	exit(1);
    }
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}