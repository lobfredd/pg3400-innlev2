#ifndef INTLIST
#define INTLIST
#include <stdio.h>

typedef struct{
	int* array;
	int length;
	int count;
	void (*add)();
	void (*remove)();
}IntList;

IntList* initIntList(int length);

void freeIntList(IntList *self);

#endif