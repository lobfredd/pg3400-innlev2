#include <stdlib.h>
#include "../headers/intlist.h"
#include <stdbool.h>

void resizeIntArray(IntList *self){
	self->length *= 2;
	self->array = realloc(self->array, self->length * sizeof(int));
	if(self->array == NULL){
		fprintf(stderr, "Out of memory?");
		exit(1);
	}
}

void addInt(IntList *self, int number){
	if(self->count >= self->length) resizeIntArray(self);
	self->array[self->count++] = number;
}

bool contains(IntList *self, int target) {
	for (int i = 0; i < self->count; i++) {
		if (self->array[i] == target) {
			return true;
		} 
	}
	return false;
}

void removeInt(IntList *self, int target) {
	if (!contains(self, target)) return;

	int* newArray = malloc(self->length * sizeof(int));
	if(newArray == NULL){
		fprintf(stderr, "Out of memory?");
		exit(1);
	}

	int newI = 0;
	for (int i = 0; i < self->count; i++) {
		if(self->array[i] != target) newArray[newI++] = self->array[i];
	}
	free(self->array);
	self->array = newArray;
	self->count = newI;	
}

IntList* initIntList(int length){
	IntList *iList = malloc(sizeof(IntList));
	if(iList == NULL){
		fprintf(stderr, "Out of memory?");
		exit(1);
	}

	if(length < 1) length = 1;
	iList->length = length;
	iList->array = malloc(iList->length * sizeof(int));
	if(iList->array == NULL){
		fprintf(stderr, "Out of memory?");
		exit(1);
	}
	iList->count = 0;
	iList->add = addInt;
	iList->remove = removeInt;
	return iList;
}

void freeIntList(IntList *self){
	free(self->array);
	free(self);
}