#ifndef STRINGUTIL
#define STRINGUTIL
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

char lowercase(char letter);
char uppercase(char letter);

bool isUpperCaseChar(char c);
bool isLowerCaseChar(char c);
bool isASCIILetter(char c);
bool isASCIINumber(char c);

char* concat(char *s1, char *s2);

#endif