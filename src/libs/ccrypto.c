#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include "../headers/stringutil.h"
#include "../headers/ccrypto.h"
#include "../headers/intlist.h"
#include "../headers/charlist.h" 

static int d = 5;

void setD(int distance){
	d = distance;
}

void printStatus(StatusCode s){
	printf("%s\n", getStatusMsg(s));
}
char* getStatusMsg(StatusCode s){
	switch(s){
		case Success:
			return SUCCESS_MSG;
		case MsgFileError:
			return MSG_FILE_ERROR_MSG;
		case KeyFileError:
			return KEY_FILE_ERROR_MSG;
		case DConditionNotMet:
			return D_CONDITION_ERROR_MSG;
		case InvalidKey:
			return INVALID_KEY_ERROR_MSG;
		default:
			return NULL;
	}
}

char* makeKey(FILE *inputStream){
	CharList* key = initCharList(100);
	char c;
	while((c = fgetc(inputStream)) != EOF) {
		if(isASCIILetter(c)){
			key->add(key, c);
		}
    }
    return detachCharArrayAndFree(key);
}

char* makeKeyFromFile(char *filename){
	FILE *f = fopen (filename, "r");
	char* key = makeKey(f);
	fclose(f);
	return key;
}

int choosePositionIn(IntList* possiblePos, int lastCharPos){
	srand(clock());
	int pos;

	int startI = rand() % possiblePos->count;
	int i = startI;
	do{
		pos = possiblePos->array[i];
		if(abs(pos-lastCharPos) >= d) break; //d condition met
		i = (i == possiblePos->count-1) ? 0 : i+1;
	}while(i != startI); //Kinda circular loop to make it random and effective

	return pos;
}

IntList* getPossiblePositions(const char* text, char c){
	int textLength = strlen(text);
	IntList* possiblePos = initIntList(textLength);
	for(int i = 0; i < textLength; i++){
		if(text[i] == c) possiblePos->add(possiblePos, i);
	}
	return possiblePos;
}

bool validIntExtracted(char* str, int length, int* dest){
	if(length > MAX_INT_STRING_LENGTH || length < 1) return false;
	errno = 0;
	long num = strtol(str, NULL, 10);
	if(errno == ERANGE) return false;
	if(num >= INT_MIN && num <= INT_MAX){
		*dest = num;
		return true;
	} 
	return false;
}

char* getCipher(const char* encoded, int length){
	if(length < 3 && encoded[0] != '[') return NULL;

	CharList* cipher = initCharList(MAX_INT_STRING_LENGTH);
	for(int i = 1; i < length && i < MAX_INT_STRING_LENGTH; i++){
		if(encoded[i] == ']' && i > 1){
			return detachCharArrayAndFree(cipher);
		}
		else if(!isASCIINumber(encoded[i])) break; //cipher not valid..
		cipher->add(cipher, encoded[i]);
	}
	freeCharList(cipher);
	return NULL;
}

StatusCode sencode(const char *msg, const char *key, char **encoded){
	StatusCode s;
	*encoded = encode(msg, key, &s);
	return s;
}

char* encode(const char* msg, const char* key, StatusCode *status){
	*status = Success; //unless another error encountered
	int lastCharPos = -1000;
	int msgLength = strlen(msg);
	CharList* encoded = initCharList(msgLength*4);

	for(int i = 0; i < msgLength; i++){
		char c = msg[i];
		if(isASCIILetter(c)){

			IntList* possiblePos = getPossiblePositions(key, lowercase(c));	
			if(possiblePos->count <= 0){
				*status = InvalidKey;
				freeIntList(possiblePos);
				freeCharList(encoded);
				return NULL;
			}
			int charIndex = choosePositionIn(possiblePos, lastCharPos);
			freeIntList(possiblePos);

			if(abs(charIndex-lastCharPos) < d) *status = DConditionNotMet;
			lastCharPos = charIndex;
			if(isUpperCaseChar(c)) charIndex = -charIndex;
			encoded->addIntAsString(encoded, "[%d]", charIndex);
		}
		else encoded->add(encoded, c);
	}
	return detachCharArrayAndFree(encoded);
}

StatusCode sdecode(const char *encoded, const char *key, char **decoded){
	StatusCode s;
	*decoded = decode(encoded, key, &s);
	return s;
}

char* decode(const char* encoded, const char* key, StatusCode *status){
	*status = Success; //unless another error encountered
	int msgLength = strlen(encoded);
	int keyLength = strlen(key);
	CharList* decoded = initCharList(msgLength/4);
	for(int i = 0; i < msgLength; i++){
		char c = encoded[i];
		char* cipher = getCipher(&encoded[i], msgLength);
		int cipherLength = (cipher != NULL) ? strlen(cipher) : 0;
		if(cipherLength > 0){
			int index;
			if(validIntExtracted(cipher, cipherLength, &index)){
				if(keyLength < abs(index)-1){
					*status = InvalidKey;
					freeCharList(decoded);
					free(cipher);
					return NULL;
				}

				c = (index < 0) ? uppercase(key[abs(index)]) : key[abs(index)];
				i += cipherLength+1;
			}
			free(cipher);
		}
		decoded->add(decoded, c);
	}
	return detachCharArrayAndFree(decoded);
}
