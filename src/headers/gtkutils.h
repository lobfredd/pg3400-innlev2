#include <gtk/gtk.h>
#include "darkthemecss.h"
typedef struct{
	GtkWidget* scrollContainer;
	GtkWidget* textView;
	GtkTextBuffer* textBuffer;
}ScrollableTextView;

typedef struct{
	GtkWidget* dialog;
	GtkWidget* progressBar;
}ProgressBarDialog;

char* openFileDialog(char* title, GtkFileChooserAction action, GtkWidget* parent);
void showMessageDialog(GtkWidget* parent, GtkMessageType type, char* msg);
ScrollableTextView createScrollableTextView();
GtkWidget* createButton(char* labelText);
char* getAllTextFromBuffer(GtkTextBuffer* buffer);
void appendTextToBuffer(ScrollableTextView view, char* string);
void scrollToBottom(ScrollableTextView view);
ProgressBarDialog createAndshowProgressBar(char* labelText, GtkWidget* parent);
void loadCss();