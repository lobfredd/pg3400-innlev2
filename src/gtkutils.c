#include "headers/gtkutils.h"
#include <string.h>

//https://www.gnu.org/software/guile-gnome/docs/gtk/html/GtkFileChooserDialog.html
char* openFileDialog(char* title, GtkFileChooserAction action, GtkWidget* parent){
	char* result = NULL;
	GtkWidget *dialog = gtk_file_chooser_dialog_new(title,
	          GTK_WINDOW(parent),
	          action, //second flag makes it weird but atleast modal..
	           "_Cancel", GTK_RESPONSE_CANCEL,
	          "_Open", GTK_RESPONSE_ACCEPT,
	          NULL);
	if(gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT){
	result = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
	}
	gtk_widget_destroy(dialog);
	return result;
}

void showMessageDialog(GtkWidget* parent, GtkMessageType type, char* msg){
	GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(parent),
	        GTK_DIALOG_DESTROY_WITH_PARENT,
	        type,
	        GTK_BUTTONS_OK,
	        "%s", msg);

	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

ScrollableTextView createScrollableTextView(){
	ScrollableTextView sTextView;
	sTextView.textView = gtk_text_view_new();
	sTextView.scrollContainer = gtk_scrolled_window_new(NULL, NULL);
	g_object_set(sTextView.scrollContainer, "expand", TRUE, NULL);

	gtk_container_add(GTK_CONTAINER(sTextView.scrollContainer), sTextView.textView);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(sTextView.textView), GTK_WRAP_WORD_CHAR);
	sTextView.textBuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(sTextView.textView));

	return sTextView;
}

GtkWidget* createButton(char* labelText){
	GtkWidget* button = gtk_button_new_with_label(labelText);
	g_object_set(button, "expand", FALSE, NULL);
	return button;
}

char* getAllTextFromBuffer(GtkTextBuffer* buffer){
	GtkTextIter start, end;
	gtk_text_buffer_get_bounds(buffer, &start, &end);
	return gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
}

void appendTextToBuffer(ScrollableTextView view, char* string){
	GtkTextIter end;
	gtk_text_buffer_get_end_iter(view.textBuffer, &end);
	gtk_text_buffer_insert(view.textBuffer, &end, string, -1);
	gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW(view.textView), &end, 0.0, FALSE, 0, 0);

	GtkAdjustment *scrollbar = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(view.scrollContainer));
	gtk_adjustment_set_upper(scrollbar, gtk_adjustment_get_upper(scrollbar) + gtk_adjustment_get_page_increment(scrollbar));
	gtk_adjustment_set_value(scrollbar, gtk_adjustment_get_upper(scrollbar));
	gtk_scrolled_window_set_vadjustment(GTK_SCROLLED_WINDOW(view.scrollContainer), scrollbar);
}

void scrollToBottom(ScrollableTextView view){
	GtkTextIter end;
	gtk_text_buffer_get_end_iter(view.textBuffer, &end);
	gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW(view.textView), &end, 0, TRUE, 1.0, 1.0);
}

ProgressBarDialog createAndshowProgressBar(char* labelText, GtkWidget* parent){
	ProgressBarDialog pbd;
	pbd.dialog = gtk_window_new(GTK_WINDOW_POPUP);
	gtk_container_set_border_width(GTK_CONTAINER(pbd.dialog), 15);
	gtk_window_set_position(GTK_WINDOW(pbd.dialog), GTK_WIN_POS_CENTER);
	gtk_window_set_modal(GTK_WINDOW(pbd.dialog), TRUE);
	gtk_window_set_transient_for(GTK_WINDOW(pbd.dialog), GTK_WINDOW(parent));
	gtk_widget_set_name(pbd.dialog, "progressBarDialog");

	GtkWidget *gbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 15);
	gtk_container_add(GTK_CONTAINER(pbd.dialog), gbox);
	pbd.progressBar = gtk_progress_bar_new();
	gtk_progress_bar_set_show_text(GTK_PROGRESS_BAR(pbd.progressBar), TRUE);

	gtk_box_pack_start(GTK_BOX(gbox), pbd.progressBar, TRUE, TRUE, 10);
	gtk_box_pack_start(GTK_BOX(gbox), gtk_label_new(labelText), TRUE, TRUE, 0);
	gtk_progress_bar_set_pulse_step(GTK_PROGRESS_BAR(pbd.progressBar), 0.1);
	gtk_widget_show_all(pbd.dialog);
	return pbd;
}

//http://www.gtkforums.com/viewtopic.php?f=3&t=988&p=72089&hilit=background+color#p72089
void loadCss(){
	GtkCssProvider *provider = gtk_css_provider_new();
	GdkDisplay *display = gdk_display_get_default();
	GdkScreen *screen = gdk_display_get_default_screen(display);
	gtk_style_context_add_provider_for_screen(screen, GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

	gsize bytes_written, bytes_read;
	const char* home = "./style.css";

	GError *error = 0;
	if(access(home, F_OK) != -1) gtk_css_provider_load_from_path(provider, g_filename_to_utf8(home, strlen(home), &bytes_read, &bytes_written, &error), NULL);
	else gtk_css_provider_load_from_data(provider, CSS_DARK_THEME, -1, &error);
	g_object_unref(provider);
}