#ifndef STINGLIST
#define STRINGLIST
#define MAX_INT_STRING_LENGTH 11
#include <stdio.h>

typedef struct StringList StringList;
struct StringList{
	char** array;
	int length;
	int count;
	void (*add)(StringList*, char*);
	int (*spaceLeft)(StringList*);
};

StringList *initStringList(int length);
void freeStringList(StringList *self);

#endif
