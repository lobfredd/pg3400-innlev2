#ifndef STATICSTRINGS
#define STATICSTRINGS

#define CREATE_SOCKET_ERROR "Could not create the socket. Please try again.\nSystem error message"
#define WRONG_HOST_ERROR "Wrong hostname. Did you type the ip address correctly?\nSystem error message"
#define SERVER_CONNECT_ERROR "There was an error connecting to the server. Are you sure the server is up and running? \nSystem error message"
#define MESSAGE_ERROR "Unable to read the message.\nSystem error message"
#define SERVER_SHUT_DOWN "The server has shut down. cChat will now exit."
#define LOCAL "localhost"

#define JOINED " has joined the chat room."
#define LEFT " has left the chat room."
#define ACCEPT_FAIL "Unable to accept the incomming connection.\nSystem error message" 
#define BIND_FAIL "Unable to bind the incomming connection.\nSystem error message"

#define CCIPHERCMD_HELP "\nUsage: cciphercmd <action-flag> <target file path> <key file path> <optional d-integer>\nflags:\n -e \t encode\n -d \t decode\n -h \t help\n"
#define CCRACKERCMD_HELP "\nUsage: ccrackercmd <action-flag> <target file path> <key folder path>\nflags:\n -h \t help\n"


#endif